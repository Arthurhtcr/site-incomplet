var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;

document.getElementById("game").appendChild(canvas);

var bgReady = false;
var bgImage = new Image();
bgImage.onload = function ()
{
    bgReady = true;
};
bgImage.src = "img/background.png";

var herosReady = false;
var herosImage = new Image();
herosImage.onload = function ()
{
    herosReady = true;
};
herosImage.src = "img/hero.png";

var monstreReady = false;
var monstreImage = new Image();
monstreImage.onload = function ()
{
    monstreReady = true;
};
monstreImage.src = "img/monster.png";






var monstre2Ready = false;
var monstre2Image = new Image();
monstre2Image.onload = function ()
{
    monstre2Ready = true;
};
monstre2Image.src = "img/monster2.png";
var monstre2 =
{
    x: 0,
    y: 0
};


let liste_monstre2 = [];
var montre_rouge_total = 0




var heros =
{
    speed: 256, // vitesse en pixels par seconde
    x: canvas.width / 2 - 16,
    y: canvas.height / 2 - 16,
};

var monstre =
{
    x: 0,
    y: 0
};

var monstresAttrapes = 0;

var touchesAppuyees = {};

addEventListener("keydown", function (e)
{
    if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
        touchesAppuyees[e.keyCode] = true;
    }
}, false);

addEventListener("keyup", function (e)

{
    if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
        delete touchesAppuyees[e.keyCode];
    }
}, false);

var reset = function ()
{    
    // Faire apparaitre un monstre au hasard
    monstre.x = 32 + (Math.random() * (canvas.width - 96));
    monstre.y = 32 + (Math.random() * (canvas.height - 96));


    if(heros.x <= (monstre.x + 32)
        && monstre.x <= (heros.x + 32)
        && heros.y <= (monstre.y + 32)
        && monstre.y <= (heros.y + 32))
    {
        reset();
    }
    if (monstresAttrapes > 9)
    {
        montre_rouge_total = 0
        liste_monstre2 = []
        if (montre_rouge_total < Math.floor(monstresAttrapes / 10))
        {
            monstre2x = 32 + (Math.random() * (canvas.width - 96));
            monstre2y = 32 + (Math.random() * (canvas.height - 96));
            liste_monstre2.push(monstre2x,monstre2y)
            montre_rouge_total++
        };
    };
};



var reset2 = function (){

    if(heros.x <= (monstre2.x + 32)
        && monstre2.x <= (heros.x + 32)
        && heros.y <= (monstre2.y + 32)
        && monstre2.y <= (heros.y + 32))

    {
        reset2();
    }
    if (monstresAttrapes > 9){
        montre_rouge_total = 0
        liste_monstre2 = []
        if (montre_rouge_total < Math.floor(monstresAttrapes / 10)){
            monstre2x = 32 + (Math.random() * (canvas.width - 96));
            monstre2y = 32 + (Math.random() * (canvas.height - 96));
            liste_monstre2.push(monstre2x,monstre2y)
            montre_rouge_total++
        };
    };
    monstre.x = 32 + (Math.random() * (canvas.width - 96));
    monstre.y = 32 + (Math.random() * (canvas.height - 96));


};







var update = function (modifier)
{
    if (38 in touchesAppuyees)
    {
        // Touche haut
        if (heros.y > 20)
        {
            heros.y -= heros.speed * modifier;
        }
    }

    if (40 in touchesAppuyees)
    {
        // Touche bas
        if (heros.y < 415)
        {
            heros.y += heros.speed * modifier;
        }
    }

    if (37 in touchesAppuyees)
    {
        // Touche gauche
        if (heros.x > 30)
        {
            heros.x -= heros.speed * modifier;
        }
    }

    if (39 in touchesAppuyees)
    {
        // Touche droite
        if (heros.x < 450)
        {
            heros.x += heros.speed * modifier;
        }
    }

    // Y a-t-il contact ?
    if(heros.x <= (monstre.x + 32)
        && monstre.x <= (heros.x + 32)
        && heros.y <= (monstre.y + 32)
        && monstre.y <= (heros.y + 32))
    {
        ++monstresAttrapes;
        reset();
    }


    if(monstresAttrapes > 9)
    {
        for (var i = 0; i < liste_monstre2.length; i++)
        {
        if(heros.x <= (liste_monstre2[i] + 32)
        && liste_monstre2[i] <= (heros.x + 32)
        && heros.y <= (liste_monstre2[i+1] + 32)
        && liste_monstre2[i+1] <= (heros.y + 32))
    {
        ++i
        --monstresAttrapes;
        reset2();
    }
}
    }
    if (monstresAttrapes > 9){
        if (montre_rouge_total < Math.floor(monstresAttrapes / 10)){
            monstre2x = 32 + (Math.random() * (canvas.width - 96));
            monstre2y = 32 + (Math.random() * (canvas.height - 96));
            liste_monstre2.push(monstre2x,monstre2y)
            montre_rouge_total++
            
        }
    }
    if (monstresAttrapes < 10){
        montre_rouge_total = 0
        liste_monstre2 = []
    }




};

var render = function ()
{
    if (bgReady)
    {
        ctx.drawImage(bgImage, 0, 0);
    }

    if (herosReady)
    {
        ctx.drawImage(herosImage, heros.x, heros.y);
    }

    if (monstreReady)
    {
        ctx.drawImage(monstreImage, monstre.x, monstre.y);
    }

    if (montre_rouge_total != 0)
    {
        for (var i = 0; i < liste_monstre2.length; i++) {
            ctx.drawImage(monstre2Image, liste_monstre2[i], liste_monstre2[i+1]);
            i++
        }
    }






    // Score
    ctx.fillStyle = "rgb(250, 250, 250)";
    ctx.font = "24px Helvetica";
    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText( " Points : " + monstresAttrapes, 32, 32);
};

var main = function ()
{
    var now = Date.now();
    var delta = now - then;
    update(delta / 1000);
    render();
    then = now;
};

reset();
var then = Date.now();
setInterval(main, 1); // Executer aussi vite que possible

reset2();
var then = Date.now();
setInterval(main, 1);